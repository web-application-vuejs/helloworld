const express = require('express')
const app = express()
const port = 3000
// Middleware
app.get('/', (req, res) => {
  console.log(req)
  res.json({
    id: 20,
    name: 'Iphone'
  })
})

app.get('/hellome', (req, res) => {
  res.send('Hello Me')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
